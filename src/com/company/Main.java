package com.company;

public class Main {

    public static void main(String[] args) {
        HashMap myHashMap = new HashMap();
        int testKey = 1;
        int testValue = 999;
        //int testValue = Integer.MAX_VALUE;

        myHashMap.put(testKey, testValue);

        long startTime = System.nanoTime();
        System.out.println("Wartość dla indeksu 1: " + myHashMap.get(1));
        long stopTime = System.nanoTime();
        System.out.println("Czas pobrania (get) elementu z HashMapy: " + ((stopTime - startTime)) + " nanosekund.");
    }
}
